# Kryo

![kryo](kryo.png)

The Kryo theme is a minimalistic theme for the Openbox window manager created by Raven.

## Install

- To use kryo, copy the kryo folder to /usr/share/themes when you're on Linux, <br>
or /usr/local/share/themes on FreeBSD. <br>
- Set the theme with Obconf or by editing the rc.xml configuration file.
